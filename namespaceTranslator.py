#!/usr/bin/python
##############################################################################################
# title           :namespaceTranslator.py
# description     :functions to translate namespaces : BEL to miriam URN
# author          :Aishwarya Alex
# date            :20160428
# version         :0.2
# usage           :python pyscript.py
# notes           :
# python_version  :2.7.10 /3.5.2
##############################################################################################

# Import the modules needed to run the script.
from future.standard_library import install_aliases
install_aliases()
import reflectclient, os, urllib, sqlite3, requests,logging,re,json
import httplib2 as http
from io import open
from reflectclient.config import EntityType
from SPARQLWrapper import SPARQLWrapper, JSON
import xml.etree.ElementTree as et
import libchebipy as cb


class namespaceTranslator:
    # LOAD NAMESPACE DICTIONARY
    def __init__(self):
        ''' Constructor for this class. '''
        try:
            cursor = self.createMeSHDB()
            self.meshCursor = cursor
            self.from_abund_dict={}

        except IndexError as dicterror:
            logging.info(str("MeSH Dictionary error"))
        # self.meshDict = dict

    def get_reference (self, referencetype, reference):
        if referencetype.lower()== "pubmed":
            referencetype= "urn:miriam:pubmed"
        elif referencetype.lower() == "online resource" and re.match("^DB[0-9]+$", reference):
            referencetype= "urn:miriam:drugbank"
        elif referencetype.lower() == "online resource" and re.match("^REACT_[0-9]+\.?", reference):
            referencetype = "urn:miriam:reactome"

        elif referencetype.lower() == "online resource" and re.match("^hsa[0-9]+", reference):
            referencetype = "urn:miriam:kegg.pathway"

        elif referencetype.lower() == "online resource" and re.match("^ATC Code: N[0-9A-Z]+$", reference):
            referencetype= "urn:miriam:atc"
            reference=reference.strip("ATC Code: ")

        elif referencetype.lower() == "online resource" and re.match("^ATC Code: DB[0-9]+$", reference):
            reference = reference.strip("ATC Code: ")
            referencetype = "urn:miriam:drugbank"

        finalreference= referencetype + ":" + reference
        return finalreference


    def queryMeSHSPARQL(self, term):
        sparql = SPARQLWrapper("http://id.nlm.nih.gov/mesh/sparql", )
        preparedQuery = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
        PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
        PREFIX mesh2015: <http://id.nlm.nih.gov/mesh/2015/>
        PREFIX mesh2016: <http://id.nlm.nih.gov/mesh/2016/>
        PREFIX mesh2017: <http://id.nlm.nih.gov/mesh/2017/>

         SELECT ?d

         FROM <http://id.nlm.nih.gov/mesh>

           WHERE {
            ?d a meshv:Descriptor .
            ?d meshv:concept ?c .
            ?c meshv:term ?term .
            ?term rdfs:label ?label .

            FILTER (REGEX(?label,\"^""" + term + """$\","i"))

           }
        """
        sparql.addCustomParameter("inference", "true")

        sparql.setQuery(preparedQuery)

        sparql.setReturnFormat(JSON)
        result = sparql.query().convert()
        meshID = None
        if len(result['results']['bindings']) > 0:
            meshResult = result['results']['bindings'][0]['d']['value'].split("/")[-1]
            meshID = meshResult if meshResult is not None else None
        return meshID

    def get_mgi_rgd_id_from_hgnc_symbol(self,hgncsymbol,id_type):

        try:
            from urllib.parse import urlparse, urlencode
            headers = {
                'Accept': 'application/json',
            }

            uri = 'http://rest.genenames.org'
            path = '/fetch/symbol/'+str(hgncsymbol)

            target = urlparse(uri + path)
            method = 'GET'
            body = ''

            h = http.Http()

            response, content = h.request(target.geturl(), method,body,headers)
            id=hgncsymbol
            if response['status'] == '200':
                # assume that content is a json reply
                # parse content with the json module
                data =json.loads(content.decode())

                #print("response status is 200")
                if data['response']['numFound']==1:
                    id=str(data['response']['docs'][0][id_type][0].split(":")[1])
                    #print (id + "in func")
                return(id)

            else:
                logging.info('Error while fetching MGI or RGD ID detected: ' + response['status'])
                return hgncsymbol
        except Exception as otherex:
            print("here : ")
            print(otherex.args)
            logging.info('Error while fetching MGI or RGD ID detected,returning hgnc symbol ')
            return hgncsymbol


    def get_locationMap_from_mapping_file(self,filename):

        mergeDict={}
        locationMap={}
        locFile=open(filename,'r')
        for line in locFile:
            locList=line.strip("\n").split("\t")
            #print (locList)
            term=locList[0]
            if len(locList)<3:
                print (locList)
            locationType=locList[1] if len(locList)==3 else ""
            mergeTerm=locList[2] if len(locList)==3 else ""
            #print (term +" --> "+mergeTerm+" # "+locationType)
            if mergeTerm!="":
                mergeDict[term]=mergeTerm
                if locationType!="":
                    locationMap[mergeTerm] = locationType
            elif locationType !="" and locList[2]=="":
                locationMap[term]=locationType
            #print(term + " --> " + mergeTerm + " # " + locationType+"\n\n")
        locationMapList=[mergeDict,locationMap]
        return locationMapList

    def getDict(self, filename):
        dict = {}
        for line in open(filename, 'r'):
            nspace = line.split()
            dict[nspace[0]] = nspace[1]
        return dict

    # LOAD reaction DICTIONARY
    def getReactionDict(self):
        reactionDict = {}
        for line in open("inputs/reactionMapping.txt", 'r'):
            nspace = line.split()
            reactionDict[nspace[0]] = nspace[1]
        return reactionDict

    def termToIdentifier(self, belnamespace, term):
        if "GO" in belnamespace:
            term = namespaceTranslator.getGoIDfromTerm(term)
        elif "CHEM" in belnamespace :
            term = namespaceTranslator.getChemicalIDfromTerm(term)
        # else:
        #     idAndElement=getIDandNamespacefromAbundance("UNKNOWN",term)
        #     term=idAndElement[0]
        return term

    # Get GOID from reflect
    def getGoIDfromTerm(self, term):
        try:
            goID = term
            # logging.info term
            reflect_client = reflectclient.ReflectClient()
            GOresult = reflect_client.get_entities(term, [EntityType.biological_process, EntityType.cellular_component,
                                                          EntityType.molecular_function])

            if len(GOresult) > 0:
                goID = GOresult[0]['entities'][0]['identifier']
            return goID
        except requests.ConnectionError as err:
            logging.info("ReflectCLient connection error: " + err)
            return goID
        except reflectclient.ReflectClientInvalidResponse as responseErr:
            logging.info("ReflectCLient connection error!! Returning term")
            return goID
        except Exception as otherex:
            logging.info("ReflectCLient error: " + otherex)
            logging.info(type(otherex))
            return goID

    def getIDandNamespacefromAbundance(self, term, elementName):
        try:
            termId = term
            idAndelement = []

            if term in self.from_abund_dict.keys():
                idAndelement=self.from_abund_dict[term]
            else:
                reflect_client = reflectclient.ReflectClient()
                # check if protein : eg: Amyloid beta peptides- abundance/ protein abundance w/o namespace
                if elementName == "PROTEIN":
                    logging.info("Connecting to reflectclient . . .")
                    POresult = reflect_client.get_entities(term, [EntityType.proteins])
                    if len(POresult) > 0:
                        logging.info(POresult)
                        termId = POresult[0]['entities'][0]['identifier']
                        entityType = POresult[0]['entities'][0]['type']
                        if entityType == '9606':
                            elementName = "PROTEIN"
                # check for GO cellular component ->Complex, biological process, moelcular function -> phenotype
                else:
                    logging.info("Checking in reflect client, protein and go")
                    GOresult = reflect_client.get_entities(term,
                                                           [EntityType.cellular_component, EntityType.biological_process,
                                                            EntityType.molecular_function])
                    if len(GOresult) > 0:
                        logging.info("Returns in GO term")
                        termId = GOresult[0]['entities'][0]['identifier']
                        entityType = GOresult[0]['entities'][0]['type']
                        if entityType == "-21" or entityType == "-23":
                            elementName = "PHENOTYPE"
                        elif entityType == "-22":
                            elementName = "COMPLEX"
                        elif entityType == "9606":
                            elementName = "PROTEIN"
                    # if element is not GO term or Protein , check in MeSH
                    else:
                        # meshID=self.getMeshIDfromTermDict(termId)
                        meshID = self.getMeshIDfromdb(self.meshCursor, termId)
                        if meshID is not None:
                            termId = meshID
                            elementName = "PHENOTYPE"

                idAndelement = [termId, elementName]
                self.from_abund_dict[term]=idAndelement

            return idAndelement
        except requests.ConnectionError as err:
            logging.info("ReflectCLient connection error: " + err)
            idAndelement = [term, elementName]
            return idAndelement
        except reflectclient.exceptions.ReflectClientConnectionError as rcError:
            logging.info("ReflectCLient connection time out : " + rcError)
            idAndelement = [term, elementName]
            return idAndelement
        except reflectclient.ReflectClientInvalidResponse as responseErr:
            logging.info("ReflectCLient connection error!! Returning term")
            idAndelement = [term, elementName]
            return idAndelement
        except Exception as otherex:
            logging.info("ReflectCLient error: " + otherex)
            logging.info(type(otherex))
            idAndelement = [term, elementName]
            return idAndelement

    #Get name from CHEBIID
    def getChemicalTermfromCHEBIID(self,chebiid):
        try:
            chebiid=chebiid.replace(" ","")
            if re.match("^CHEBIID:[0-9]+$", chebiid) is not None:
                c=cb.ChebiEntity(chebiid)

            elif re.match("^[0-9]+$",chebiid):
                chebiid="CHEBI:"+chebiid
                c = cb.ChebiEntity(chebiid)
            return c.get_name()
        except Exception as otherEx:
            logging.info("CEHBI library error.. ignoring entity")
            return None

    # Get pubchemID from reflect
    def getChemicalIDfromTerm(self, term):
        try:
            chemID = term
            reflect_client = reflectclient.ReflectClient()
            chemResult = reflect_client.get_entities(term, [EntityType.chemicals])
            if len(chemResult) > 0:
                chemID = chemResult[0]['entities'][0]['identifier']
            return chemID
        except requests.ConnectionError as err:
            logging.info("ReflectCLient connection error: " + err)
            return chemID
        except reflectclient.ReflectClientInvalidResponse as responseErr:
            logging.info("ReflectCLient connection error: ")
            return chemID
        except Exception as otherex:
            logging.info("ReflectCLient error: " + otherex)
            logging.info(type(otherex))
            return chemID
        # Get meshID from from Dictionary # FASTER

    def getMeshIDfromdb(self, meshcursor, term):
        try:
            parameter = [term.lower()]
            meshcursor.execute("SELECT meshID FROM meshTermID WHERE term=?;", parameter)
            row = meshcursor.fetchone()
            if row is not None:
                return row[0]
            else:
                return None

        except sqlite3.Error as sqr:
            logging.info("Sqlite3 Error", sqr)
        except Exception as ex:
            logging.info(ex, ex.args)

    def getMeshIDfromTermSPARQL(self, term):
        meshID = self.queryMeSHSPARQL(term)
        return meshID if meshID is not None else None

    # Get meshID from from Dictionary # FASTER
    def getMeshIDfromTermDict(self, term):
        mesh_id = term
        lowerTerm = term.lower()
        mDict = self.meshDict  # namespaceTranslator.getMeSHDict()
        mesh_id = mDict.get(lowerTerm, term)
        # mesh_id = mDict[lowerTerm] if mDict.has_key(lowerTerm) is True else term
        return mesh_id

    def getMeSHDictFromDesc2012XML(self):
        dict = {}
        xmlfilename = "inputs/desc2012.xml"
        meshtextFilename = "inputs/mesh_desc2012.txt"
        try:
            # input file fetch from :https://www.nlm.nih.gov/mesh/2017/download/2017MeshTree.txt
            logging.info("Creating MeSH Dictionary ...")
            if not os.path.exists(xmlfilename):
                logging.info("Local copy does not exist, Downloading local copy of Mesh2012 desc2012.xml ... ")
                urllib.request.urlretrieve("ftp://nlmpubs.nlm.nih.gov/online/mesh/2012/xmlmesh/desc2012.xml",
                                           xmlfilename)

            if not os.path.exists(meshtextFilename):
                logging.info (
                    "Creating text file, to store extracted terms and uniqueID from xml, to be used in future runs ...")
                txtFile = open(meshtextFilename, 'w')
                tree = et.parse(xmlfilename)
                root = tree.getroot()
                for descriptorRec in root.findall("DescriptorRecord"):
                    meshID = descriptorRec.find("DescriptorUI").text
                    descName = descriptorRec.find("DescriptorName").find("String")  # .text.encode('utf-8')
                    # logging.info descName, meshID
                    txtFile.write(descName + "\t" + meshID + "\n")
                    conceptlist = descriptorRec.find("ConceptList")
                    for concept in conceptlist.findall("Concept"):
                        termList = concept.find("TermList")
                        for term in termList.findall("Term"):
                            altTerm = term.find("String")  # .text.encode('utf-8')
                            if altTerm != descName:
                                # logging.info altTerm,meshID
                                txtFile.write(altTerm + "\t" + meshID + "\n")
                txtFile.close()
            else:
                for line in open(meshtextFilename, 'r'):
                    # line = line.replace('\x00', '')
                    line = line.strip()
                    nspace = line.split("\t")
                    if len(nspace) == 2:
                        meshID = nspace[1]
                        term = nspace[0].lower()
                        dict[term] = meshID

        except IndexError as dicterror:
            logging.info(str("MeSH Dictionary error"))
        except Exception as otherError:
            logging.info(" !!!EXCEPTION!!! : " + str(otherError.args))
            logging.info(otherError, type(otherError))
        return dict

    def getMeSHDictFromMTree(self):
        dict = {}
        filename = "inputs/2017MeshTree.txt"
        try:
            # input file fetch from :https://www.nlm.nih.gov/mesh/2017/download/2017MeshTree.txt
            if not os.path.exists(filename):
                urllib.request.urlretrieve("https://www.nlm.nih.gov/mesh/2017/download/2017MeshTree.txt", filename)

            for line in open(filename, 'r'):
                line = line.replace('\x00', '')
                line = line.strip()
                nspace = line.split("\t")
                if len(nspace) == 3:
                    meshID = nspace[1]
                    term = nspace[2].lower()
                    dict[term] = meshID

        except IndexError as dicterror:
            logging.info(str("MeSH Dictionary error"))
        return dict

    def createMeSHDB(self):
        xmlfilename = "inputs/desc2012.xml"
        meshtextFilename = "inputs/mesh_desc2012.txt"
        try:
            # input file fetch from :https://www.nlm.nih.gov/mesh/2017/download/2017MeshTree.txt
            logging.info("Connecting MeSH sqliteDB ...")
            if not os.path.exists(xmlfilename):
                logging.info("Local copy does not exist, Downloading local copy of Mesh2012 desc2012.xml ... ")
                urllib.request.urlretrieve("ftp://nlmpubs.nlm.nih.gov/online/mesh/2012/xmlmesh/desc2012.xml",
                                           xmlfilename)
            logging.info ("Establishing connection to MeSHDB ....")
            if not os.path.exists(meshtextFilename):
                logging.info (
                    "Creating text file, to store extracted terms and uniqueID from xml, to be used in future runs ...")
                txtFile = open(meshtextFilename, 'w')
                tree = et.parse(xmlfilename)
                root = tree.getroot()
                for descriptorRec in root.findall("DescriptorRecord"):
                    meshID = descriptorRec.find("DescriptorUI").text
                    descName = descriptorRec.find("DescriptorName").find("String")  # .text.encode('utf-8')
                    # logging.info descName, meshID
                    txtFile.write(descName + "\t" + meshID + "\n")
                    conceptlist = descriptorRec.find("ConceptList")
                    for concept in conceptlist.findall("Concept"):
                        termList = concept.find("TermList")
                        for term in termList.findall("Term"):
                            altTerm = term.find("String")  # .text.encode('utf-8')
                            if altTerm != descName:
                                # logging.info altTerm,meshID
                                txtFile.write(altTerm + "\t" + meshID + "\n")
                txtFile.close()
            conn = sqlite3.connect("inputs/xbel2celld.db")
            conn.text_factory = str
            cursor = conn.cursor()
            conn.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='meshTermID';""")
            rows = cursor.fetchall()
            if len(rows) == 0:
                logging.info ("Database doesn't exist, creating ....")
                cursor.execute("""CREATE TABLE IF NOT EXISTS meshTermID (
                        term text PRIMARYKEY,
                        meshID text
                        );""")
                for line in open(meshtextFilename, 'r', encoding='utf-8'):
                    # line = line.replace('\x00', '')
                    line = line.strip()
                    nspace = line.split("\t")
                    if len(nspace) == 2:
                        meshID = nspace[1]
                        term = nspace[0].lower()
                        # logging.info term,meshID
                        parameters = [term, meshID]
                        cursor.execute("""INSERT INTO meshTermID (term,meshID) VALUES (?,?);""", parameters)
            conn.commit()
            conn.close()
            conn = sqlite3.connect("inputs/xbel2celld.db")
            conn.text_factory = str
            cursor = conn.cursor()
            logging.info ("Connected to MeSHDB ....")

            return cursor
        except sqlite3.Error as sqr:
            logging.info("Sqlite3 Error", sqr)
        except Exception as ex:
            logging.info(ex, ex.args)


# NT=namespaceTranslator()
# NT.get_mgi_rgd_id_from_hgnc_symbol("RETN","mgd_id")
# NT.get_mgi_rgd_id_from_hgnc_symbol("RETN","rgd_id")