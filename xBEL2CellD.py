#!/usr/bin/python
##############################################################################################
# title           :xBEL2CellD.py
# description     :This script will generate 2 tab delimited files(reactions.txt, nodes.txt) from an .XBEL file
# author          :Aishwarya Alex
# version         :0.2
# usage           :python pyscript.py
# notes           :
# python_version  :2.7.10 /3.5.2
##############################################################################################

# Import the modules needed to run the script.
import string,reflectclient, logging,datetime,re,pandas
import namespaceTranslator as NTL
import xml.etree.ElementTree as ET

from reflectclient.config import EntityType


class xBEL2CellD:


    def __init__(self):
        self.NT= NTL.namespaceTranslator()
        #self.mDict = self.NT.meshDict
        self.meshCursor=self.NT.meshCursor
        self.reflect_client = reflectclient.ReflectClient()
        self.nodeDict={}
        #self.nodeFrame=pandas.DataFrame(columns=['IDENTIFIER','TYPE','NAME','URN','ANNOTATIONS','CELL','TISSUE','COMPARTMENT','OTHER_ANNOTATION'])



    # func to get parameters(function, name and namespace) for elements (any node SUBJECT,OBJECT, MODIFIER(if nested))
    def get_details_from_node(self,speciesMap, node,locationAnnotation,otherAnnotation):
        try:
            returnVal = []
            term = node.find(self.ns_tag("term"))
            leafTerm = self.get_leafTerm(term)
            nestedDict = self.NT.getDict("inputs/belSpeciesNestedAnnotationsMapping.txt")
            nodeEntry = self.get_entry_for_term(leafTerm)
            if nodeEntry is not None:
                elementAnno=""
                if leafTerm !=term:
                    element = term.get(self.ns_tag('function')) if term.get(self.ns_tag('function')) is not None else ""
                    elementAnno=nestedDict.get(element) if nestedDict.get(element)is not None else ""
                    if elementAnno == "":
                       self.check_to_add_in_file(element, "outputs/additional/skippedLevels.txt")
                nodeEntry = nodeEntry + "\t" + elementAnno+"\t"+locationAnnotation
                returnVal = self.getSpecies(nodeEntry, speciesMap,otherAnnotation)  # add entry or retrieve existing Id
                return returnVal
            else:
                return None
        except Exception as detailsEx:
            logging.info("!!! FUNC:get_details_from_node!!!")
            logging.info(str(detailsEx.args))

    # GET DETAILS FROM TERM FUNCTION AND PARameter
    def get_entry_for_term(self,term):
        try:
            entry = ""
            entityDict = self.NT.getDict("inputs/entityMapping.txt")
            belActivityDict=self.NT.getDict("inputs/belSpeciesNestedAnnotationsMapping.txt")
            abundanceDict = self.NT.getDict("inputs/abundanceMapping.txt")
            namespaceDict = self.NT.getDict("inputs/namespaceMapping.txt")
            element = term.get(self.ns_tag('function')) if term.get(self.ns_tag('function')) is not None else ""
            parameter = term.find(self.ns_tag("parameter"))
            belnamespace = parameter.get(self.ns_tag("ns")) if parameter.get(self.ns_tag("ns")) is not None else ""
            miriamnamespace = ""
            identifier = parameter.text
            # logging.info bel values element,parameter,identifier
            logging.info(" BEL:: #element: " + element + " #identifier: " + str(identifier) + " #namespace: " + belnamespace)

            #IF SNP ignore node and reaction
            if belnamespace =="dbSNP":
                logging.info("This version does not handle SNPS .. Moving to next reaction")
                return None

            if element in belActivityDict.keys() and belnamespace =="HGNC:":
                element="PROTEIN"
                logging.info("BEL entity missing a level, setting entity as PROTEIN")

            elementName = element  # function@TERM
            elementAnnotation = ""  # ns@PARAMETER+text
            # get element

            if element in list(entityDict.keys()):
                elementName = entityDict[element]

            else:
                # add not handled entity to file
                self.check_to_add_in_file(element, "outputs/additional/toMapEntities.txt")
                elementName="abundance"

            # check for abundance element name from namespace
            if element == "abundance" or belnamespace == "":
                if belnamespace is not "":
                    new_element = element + "_" + belnamespace
                    if re.match("^[0-9]$", identifier) is not None and belnamespace == "CHEBIID":
                        chemName = self.NT.getChemicalTermfromCHEBIID(identifier)
                        identifier = chemName if chemName is not None else None
                    if new_element in abundanceDict:
                        element = abundanceDict[new_element]
                    if "MESH" in belnamespace:
                        #id=self.NT.getMeshIDfromTermSPARQL(identifier)
                        id = self.NT.getMeshIDfromdb(self.meshCursor, identifier)

                        if id is not None:
                            identifier=id
                            elementName = "PHENOTYPE"

                    elif belnamespace  not in ('SCHEM','CHEBI','CHEBIID'):
                        idAndElement = self.NT.getIDandNamespacefromAbundance(identifier, elementName)
                        identifier = idAndElement[0]
                        elementName = idAndElement[1]
                        self.check_to_add_in_file(belnamespace, "outputs/additional/toAddNamespaces.txt")
                else:  # no namespace, checking in G0, eg encountered: neuroinflammation,Amyloid beta peptides, etc
                    elementName = "UNKNOWN"
                    # identifier = NT.termToIdentifier(belnamespace, parameter.text)
                    idAndElement = self.NT.getIDandNamespacefromAbundance(identifier, elementName)
                    identifier = idAndElement[0]
                    elementName = idAndElement[1]
                    logging.info(identifier+" "+elementName)
                    if re.match("^GO:[0-9]+$", str(identifier)) is not None:
                        belnamespace = "GO"
                    elif re.match("^D[0-9]+$", str(identifier)) is not None:
                        belnamespace = "MESH"
                    elif identifier.startswith("ENSP0000"):
                        belnamespace = "ENSMBL"


            elif "MESH" in belnamespace or "GO" in belnamespace:
                if "MESH" in belnamespace:
                    #id = self.NT.getMeshIDfromTermSPARQL(identifier)
                    id= self.NT.getMeshIDfromdb(self.meshCursor,identifier)
                    if id is not None:
                        identifier = id
                        elementName="PHENOTYPE"
                else:
                    idAndElement = self.NT.getIDandNamespacefromAbundance(identifier, elementName)
                    identifier = idAndElement[0]
                    elementName = idAndElement[1]

                if identifier.startswith("GO:"):
                    belnamespace = "GO"
                elif re.match("^D[0-9]+$", str(identifier)) is not None:
                    belnamespace = "MESH"
                elif parameter.text == identifier:
                    belnamespace=""



            elif belnamespace is not "":
                identifier = self.NT.termToIdentifier(belnamespace, parameter.text)

            if (belnamespace in namespaceDict) is not True and belnamespace !="":
                # if namespaceDict.has_key(belnamespace) is False or belnamespace is not "":
                idAndElement = self.NT.getIDandNamespacefromAbundance(identifier, element)
                identifier = idAndElement[0]
                elementName = idAndElement[1]
                if elementName in list(entityDict.keys()):
                    elementName = entityDict[element]
                    # add not handled entity to file
                self.check_to_add_in_file(element, "outputs/additional/toMapEntities.txt")

                if identifier.startswith("GO:"):
                    belnamespace = "GO"
                elif re.match("^D[0-9]+$", str(identifier)) is not None:
                    belnamespace = "MESH"

                elif identifier.startswith("ENSP0000"):
                    belnamespace="ENSMBL"
                else:
                    belnamespace=""
                    #miriamnamespace="urn:miriam:ensembl"
                self.check_to_add_in_file(belnamespace, "outputs/additional/toAddNamespaces.txt")

            if belnamespace in namespaceDict:
                miriamnamespace = namespaceDict[belnamespace]

            finalnamespace = miriamnamespace
            if "MGI" in belnamespace:
                identifier=identifier.upper()
                identifier=self.NT.get_mgi_rgd_id_from_hgnc_symbol(identifier,"mgd_id")
                finalnamespace = finalnamespace+":"+identifier

            elif "RGD" in belnamespace:
                identifier = identifier.upper()
                identifier = self.NT.get_mgi_rgd_id_from_hgnc_symbol(identifier, "rgd_id")
                finalnamespace = finalnamespace+":"+identifier

            elif "SCHEM" in belnamespace or "CHEBI" == belnamespace:
                finalnamespace = ""
                elementName="SIMPLE_MOLECULE"
            elif finalnamespace is not "":
                finalnamespace = finalnamespace + ":" + identifier


            if identifier is not None:
                elementAnnotation = parameter.text + "\t" + finalnamespace
                entry = elementName + "\t" + elementAnnotation
                return entry
            else:
                logging.info("Entity not distinguishable... Ignoring")
                return None
        except Exception as ExceptionTerm:
            logging.info("!!! FUNC:get_entry_for_term!!!")
            logging.info(str(term), str(belnamespace))
            logging.info(str(ExceptionTerm.args))
            return None



    # tagNamespacePrefix='{http://belframework.org/schema/1.0/xbel}'
    # function to attach namespace to tag
    def ns_tag(self, tag):
        return str(ET.QName('{http://belframework.org/schema/1.0/xbel}' + tag))



    # function to remove namespace from tag
    def remove_ns_tag(self, tag):
        return string.replace(str(tag), '{http://belframework.org/schema/1.0/xbel}', '')



    def check_to_add_in_file(self, entry, filename):
        alreadyIn = [line.strip() for line in open(filename, "r")]
        if entry not in alreadyIn:
            f = open(filename, "a")
            f.write(entry + "\n")
            f.close()

    # function to get annotationMap
    def get_annotation_details(self, nodeAnnoGroup):
        annoMap = {}
        for anno in nodeAnnoGroup.iter(tag=self.ns_tag('annotation')):
            # logging.info (anno.get(ns_tag("refID")).upper(),anno.text)
            annoMap[anno.get(self.ns_tag("refID")).upper()] = anno.text
        return annoMap

    # function to get annotation elelment from map
    def get_annotation_from_element(self, annoMap, annoElement):
        keyslist=annoMap.keys()
        anno=""
        for each in keyslist:
            if annoElement.lower() in str(each).lower():
                anno=annoMap.get(each)

        return anno

    # function to find leafterm (term with function and parameter
    def get_leafTerm(self, term):
        leafterm = term
        element = term.get(self.ns_tag('function')) if term.get(self.ns_tag("function")) is not None else None
        parameter = term.find(self.ns_tag("parameter")) if term.find(self.ns_tag("parameter")) is not None else None
        if element is not None and parameter is None:
            nextTerm = term.find(self.ns_tag("term"))
            return self.get_leafTerm(nextTerm)
        return leafterm

        # function to check id species exists, if not create entry, else return id
        # function to get subject and object (and modifier) enteries.
    def getSpecies(self, nodeEntry, speciesDict,otherAnnotation):
        nodeFile = "outputs/nodes.txt"
        speciesId = speciesDict.get(nodeEntry)
        # logging.info "speciesId retrieved :"+str(speciesId)
        totalSpecies = len(speciesDict)
        returnVal = []
        if speciesId is None:
            totalSpecies = totalSpecies + 1
            speciesId = "s" + str(totalSpecies)
            speciesDict[nodeEntry] = speciesId
            nodeFile = open("outputs/nodes.txt", 'a')
            nodeEntry = speciesId + "\t" + nodeEntry + "\t" +  ";".join(otherAnnotation) +"\n"
            nodeFile.write(nodeEntry)
            nodeFile.close()
            nodeEntry.strip("\n")
            nodeInfoList=nodeEntry.split("\t")
            if len(nodeInfoList)==10:
                #self.nodeFrame=self.nodeFrame.append(nodeInfoList,ignore_index=True)
                logging.info("Node columns matching")
            else:
                logging.info("Number of column entries not as expected!!\n Required: 10\n Encounter: "+str(len(nodeInfoList)))
                exit()
        logging.info(speciesId + " : " + nodeEntry)
        returnVal.append(speciesId)
        returnVal.append(speciesDict)
        return returnVal

    # Main function to convert- starts here
    def convertXBEL(self, filepath):

        # create new output file 1.nodes.txt 2.reactions.txt
        logging.info(filepath)
        nodeFile = "outputs/nodes.txt"
        nFile = open(nodeFile, "w")
        #nFile.write("IDENTIFIER\tTYPE\tNAME\tURN\tANNOTATIONS\tLOCATION\tCELL\tTISSUE\tCELLSTRUCTURE\tCELLLINE\tOTHER_ANNOTATION\n")
        nFile.write("IDENTIFIER\tTYPE\tNAME\tURN\tANNOTATIONS\tCELL\tTISSUE\tCOMPARTMENT\tORGANISM\tOTHER_ANNOTATION\n")
        nFile.close()
        reactionFile = "outputs/reactions.txt"
        rFile = open(reactionFile, "w")
        #rFile.write("IDENTIFIER\tTYPE\tREACTANTS\tMODIFIERS\tPRODUCTS\tMODIFIER_TYPE\tREFERNCE\tSPECIES\tDISEASE\tCELL\tTISSUE\tCELLSTRUCTURE\tCELLLINE\tOTHER_ANNOTATION\n")
        rFile.write("IDENTIFIER\tTYPE\tREACTANTS\tMODIFIERS\tPRODUCTS\tMODIFIER_TYPE\tREFERENCE\tSPECIES\tDISEASE\tCELL\tTISSUE\tCOMPARTMENT\tORGANISM\tOTHER_ANNOTATION\n")
        rFile.close()
        try:
            tree = ET.parse(filepath)
            root = tree.getroot()

            # create new files
            f = open("outputs/additional/toMapEntities.txt", "w")
            f.close()

            f = open("outputs/additional/toMapReactions.txt", "w")
            f.close()

            # LOAD namespace DIctionary
            namespaceDict = self.NT.getDict("inputs/namespaceMapping.txt")
            reactionDict = self.NT.getDict("inputs/reactionMapping.txt")
            l = self.NT.get_locationMap_from_mapping_file("inputs/locationMapping.txt")
            mergeMap = l[0]
            locationTypeMap = l[1]


            # create dictionary for species
            speciesMap = {}
            # get all statement nodes, annotation and sub nodes (complex, or composite structures)
            reactionIdentifier = 0
            belStatementNumber=0
            expStatementFile = "outputs/additional/exceptionalStatements.txt"
            file = open(expStatementFile, 'w')
            file.write("STATEMENT_NUMBER\tRELATIONSHIP\tCAUSE\tREACTANT\tPRODUCT\tMODIFIER\tEVIDENCE\n")
            file.close()
            #evidence = ""
            for index, statementGr in enumerate(root.iter(tag=self.ns_tag('statementGroup'))):
                for statement_num, statement in enumerate(statementGr.findall(self.ns_tag('statement'))):
                    logging.info("\n\n\n############################################")
                    logging.info("GROUP" + str(index + 1) + " :: STATEMENT  " + str(statement_num + 1))
                    logging.info("############################################")
                    belStatementNumber += 1  # statement number

                    ##annotation for evidence statement: pubmedid, journal, evidence , species , cellLine
                    annoGroup = statement.find(self.ns_tag('annotationGroup'))
                    # get_node_info_recursive(annoGroup)
                    ##get info on the relationship -CURRENTLY :only reference, AVIALABLE :species,and diseases, cellline..
                    relationship = statement.get(self.ns_tag("relationship"))  # if statement.get(ns_tag("relationship")) is not None else None)
                    evidence = annoGroup.find(self.ns_tag("evidence")).text if annoGroup is not None else evidence
                    citation = annoGroup.find(self.ns_tag("citation")) if annoGroup is not None else citation
                    referenceType = citation.get(self.ns_tag("type")) if annoGroup is not None else referenceType
                    reference = citation.find(self.ns_tag("reference")).text if annoGroup is not None else reference
                    #referenceType = namespaceDict.get(referenceType) if namespaceDict.get(referenceType) is not None else referenceType
                    finalreference = self.NT.get_reference(referenceType, reference)

                    annoMap = self.get_annotation_details(annoGroup) if annoGroup is not None else annoMap
                    species = self.get_annotation_from_element(annoMap, "SPECIES")
                    disease = self.get_annotation_from_element(annoMap, "DISEASE")
                    cell = self.get_annotation_from_element(annoMap, "CELL")
                    tissue = self.get_annotation_from_element(annoMap, "TISSUE")
                    cellline = self.get_annotation_from_element(annoMap, "CELLLINE")
                    cellstructure = self.get_annotation_from_element(annoMap, "CELLSTRUCTURE")

                    compartment_f=""
                    cell_f=""
                    tissue_f=""
                    organism_f=""

                    for each in (cell, cellstructure, cellline, tissue,species):
                        term=mergeMap[each] if each in mergeMap.keys() else each
                        locType=locationTypeMap[term] if term in locationTypeMap.keys() else ""
                        if locType=="COMPARTMENT":
                            compartment_f=term if compartment_f =="" or compartment_f==term else compartment_f+";"+term
                        elif locType=="CELL":
                            cell_f=term if cell_f =="" or cell_f==term else cell_f+";"+term
                        elif locType=="TISSUE":
                            tissue_f=term if tissue_f =="" or tissue_f==term else tissue_f+";"+term
                        elif locType=="ORGANISM":
                            organism_f=term if organism_f =="" or organism_f==term else organism_f+";"+term


                    keyList = list(annoMap.keys())
                    otherKeys = set(keyList) - set(['SPECIES', 'DISEASE', 'CELL', 'TISSUE', 'CELLSTRUCTURE', 'CELLLINE'])
                    otherAnnotation = [each + ":" + annoMap[each] for each in otherKeys]
                    # logging.info otherKeys
                    for each in (otherKeys):
                        term=each+":"+annoMap[each]
                        term=mergeMap[term] if term in mergeMap.keys() else term
                        locType=locationTypeMap[term] if term in locationTypeMap.keys() else ""
                        if locType=="COMPARTMENT":
                            compartment_f=term if compartment_f =="" or compartment_f==term else compartment_f+";"+term
                        elif locType=="CELL":
                            cell_f=term if cell_f =="" or cell_f==term else cell_f+";"+term
                        elif locType=="TISSUE":
                            tissue_f=term if tissue_f =="" or tissue_f==term else tissue_f+";"+term
                        elif locType=="ORGANISM":
                            organism_f=term if organism_f =="" or organism_f==term else organism_f+";"+term


                    logging.info("EVIDENCE: " + evidence)
                    reactant = ""
                    modifier = ""
                    product = ""
                    modifierType = ""
                    # nFile = open(nodeFile, "a")
                    subject = statement.find(self.ns_tag("subject"))
                    object = statement.find(self.ns_tag("object"))
                    nested = object.find(self.ns_tag("statement")) if object is not None else None
                    # IF RELATIONSHIP IS EMPTY, check for subject node type is reaction
                    if relationship is None:
                        relationship = subject.find(self.ns_tag('term')).get(self.ns_tag('function'))
                        alreadyIn = [line.strip() for line in open("outputs/additional/toMapReactions.txt", "r")]
                        if relationship not in alreadyIn:
                            f = open("outputs/additional/toMapReactions.txt", "a")
                            f.write(relationship + "\n")
                            f.close()
                        # if reaction is ComplexAbundance
                        if relationship == "complexAbundance":
                            logging.info("BEL relationship COMPLEX ABUNDANCE, standalone entity in SBML. Ignoring...")

                            f = open(expStatementFile, 'a')
                            #f.write("RELATIONSHIP: " + str(relationship) + "|" + evidence + "\n")
                            reactionEntry =str(belStatementNumber) +"\t"+str(relationship) +"\t"+"COMPLEXABUNDANCE\t" + reactant + "\t " + product + "\t" +modifier+"\t"+finalreference +" | "+evidence.replace("\n"," ").replace("\t"," ")+"\n"
                            f.write(reactionEntry)
                            f.close()
                            # Do nothing --ignore standalone complex node
                            continue

                    # if reaction is reaction: then get list of reactnats and products
                    elif relationship == 'reaction':
                        logging.info("BEL relation :REACTION, not convertable")
                        f = open(expStatementFile, 'a')
                        #f.write("RELATIONSHIP: "+str(relationship) + "|" + evidence + "\n")
                        reactionEntry = str(belStatementNumber) + "\t" + str(relationship) + "\t"+"REACTION\t" + reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace("\n"," ").replace("\t"," ") + "\n"
                        f.write(reactionEntry)
                        f.close()
                        # Do nothing --not translatable
                        continue



                    else:
                        logging.info("------RELATIONSHIP------\n" + relationship)

                        # entityDict = NT.getdict("inputs/entityMapping.txt")
                        # OBJECT IS NOT NONE : Statement has at least reactant and product
                        if object is not None:
                            if nested is not None:
                                # logging.info "Get modifier, subject and object"
                                try:
                                    modObject = subject
                                    subject = nested.find(self.ns_tag("subject"))
                                    object = nested.find(self.ns_tag("object"))
                                    modifierType =relationship
                                    relationship = nested.get(self.ns_tag("relationship"))
                                    locationAnnotation = cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f
                                    logging.info("MODIFIER :")
                                    returnedVal = self.get_details_from_node(speciesMap, modObject,locationAnnotation,otherAnnotation)
                                    speciesMap = returnedVal[1]
                                    modifier = returnedVal[0]
                                    if modifierType in reactionDict.keys():
                                        logging.info("modifier-modifyingrealtionship-(subject-relationship-object) => modifer-modifyingrealtionship-subject ; subject-relationship-object")
                                        modifierType=reactionDict[modifierType]
                                        logging.info("REACTANT :")
                                        returnedVal = self.get_details_from_node(speciesMap, modObject,locationAnnotation,otherAnnotation)
                                        if returnedVal is None:
                                            continue
                                        speciesMap = returnedVal[1]
                                        reactant = returnedVal[0]
                                        logging.info("PRODUCT :")
                                        returnedVal = self.get_details_from_node(speciesMap, subject,locationAnnotation,otherAnnotation)
                                        if returnedVal is None:
                                            continue
                                        speciesMap = returnedVal[1]
                                        product = returnedVal[0]
                                        reactionIdentifier += 1
                                        reactionEntry = "r" + str(
                                            reactionIdentifier) + "\t" + modifierType + "\t" + modifier + "\t" + ""+ "\t" + reactant  + "\t" + "" + "\t" +finalreference + "\t" + species + "\t"+disease + "\t"  + locationAnnotation + "\t" + ";".join(otherAnnotation) + "\n"
                                        logging.info(reactionEntry)
                                        reactionFile = open("outputs/reactions.txt", 'a')
                                        reactionFile.write(reactionEntry)
                                        reactionFile.close()
                                        reactionIdentifier += 1
                                        reactionEntry = "r" + str(
                                            reactionIdentifier) + "\t" + relationship + "\t" + reactant + "\t" + "" + "\t" + product + "\t" + "" + "\t" +finalreference + "\t" + species + "\t" + disease + "\t" + locationAnnotation + "\t" + ";".join(
                                            otherAnnotation) + "\n"
                                        logging.info(reactionEntry)
                                    else:
                                        logging.info("Modifying relationship not translatable.. skipping modifier")
                                        modifierType=""
                                        modifier=""


                                except Exception as nestedException:
                                    logging.info("!!! EXCEPTION in nested statement!!!")
                                    logging.info(str(nestedException.args))
                                    f = open(expStatementFile, 'a')
                                    #f.write("RELATIONSHIP: " + str(relationship) + "|" + evidence + "\n")
                                    reactionEntry = str(belStatementNumber) + "\t" + str(relationship) +"\tNODE is SNP or undetified\t" + reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace("\n"," ").replace("\t"," ") + "\n"
                                    f.write(reactionEntry)
                                    f.close()
                                    # exit()
                            #if object is a TRANSLOCATION
                            translocationNode=object.find(self.ns_tag("term"))
                            if translocationNode.get(self.ns_tag('function'))=="translocation":
                                try:
                                    logging.info("Translocation to state transition: Setting relationship to STATE_TRANSITION with subject as modifier")
                                    modifierType="MODIFIER_"+relationship
                                    relationship="translocation"
                                    modObject=subject
                                    logging.info("MODIFIER : ")
                                    locationAnnotation = cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f

                                    returnedVal = self.get_details_from_node(speciesMap, modObject,locationAnnotation,otherAnnotation)
                                    if returnedVal is None:
                                        modifier=""
                                    else:
                                        speciesMap = returnedVal[1]
                                        modifier = returnedVal[0]
                                    locationList=translocationNode.findall(self.ns_tag("parameter"))
                                    if len(locationList)==2:
                                        logging.info("REACTANT :")
                                        location=locationList[0].text

                                        term = mergeMap[location] if location in mergeMap.keys() else location
                                        locType = locationTypeMap[term]
                                        if locType == "COMPARTMENT":
                                            compartment_f = term
                                        elif locType == "CELL":
                                            cell_f = term
                                        elif locType == "TISSUE":
                                            tissue_f = term
                                        elif locType == "ORGANISM":
                                            organism_f = term

                                        locationAnnotation=cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f
                                        # locationAnnotation = location + "\t" + annoEntry
                                        returnedVal = self.get_details_from_node(speciesMap, translocationNode,locationAnnotation,otherAnnotation)
                                        speciesMap = returnedVal[1]
                                        reactant = returnedVal[0]
                                        logging.info("PRODUCT :")
                                        location = locationList[1].text
                                        term = mergeMap[location] if location in mergeMap.keys() else location
                                        locType = locationTypeMap[term]
                                        if locType == "COMPARTMENT":
                                            compartment_f = term
                                        elif locType == "CELL":
                                            cell_f = term
                                        elif locType == "TISSUE":
                                            tissue_f = term
                                        elif locType == "ORGANISM":
                                            organism_f = term
                                        locationAnnotation = cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f
                                        returnedVal = self.get_details_from_node(speciesMap, translocationNode,locationAnnotation,otherAnnotation)
                                        speciesMap = returnedVal[1]
                                        product = returnedVal[0]
                                    else:
                                        logging.info("Translation node incomplete, eg: no information on location")
                                        f = open(expStatementFile, 'a')
                                        # f.write("RELATIONSHIP: " + str(relationship) + "|" + evidence + "\n")
                                        reactionEntry = str(belStatementNumber) + "\t" + str(
                                            relationship) + "\t" +"LOCATION MISSING FOR A TRANSLOCATED NODE\t"+ reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace(
                                            "\n", " ").replace("\t", " ") + "\n"
                                        f.write(reactionEntry)
                                        f.close()
                                        continue
                                except Exception as translocationEx:
                                    logging.info("!!! EXCEPTION in translocation statement!!!")
                                    logging.info( str(translocationEx.args))
                                    logging.info(type(translocationEx))
                                    f = open(expStatementFile, 'a')
                                    # f.write("RELATIONSHIP: " + str(relationship) + "|" + evidence + "\n")
                                    reactionEntry = str(belStatementNumber) + "\t" + str(
                                        relationship) + "\tNODE is SNP or undetified\t" + reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace(
                                        "\n", " ").replace("\t", " ") + "\n"
                                    f.write(reactionEntry)
                                    f.close()


                            else:
                                # REACTANT AND PRODUCTS
                                logging.info("REACTANT :")
                                locationAnnotation = cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f
                                returnedVal = self.get_details_from_node(speciesMap, subject,locationAnnotation,otherAnnotation)
                                if returnedVal is None:
                                    continue
                                speciesMap = returnedVal[1]
                                reactant = returnedVal[0]
                                logging.info("PRODUCT :")
                                locationAnnotation = cell_f + "\t" + tissue_f + "\t" + compartment_f+"\t"+organism_f
                                returnedVal = self.get_details_from_node(speciesMap, object,locationAnnotation,otherAnnotation)
                                if returnedVal is None:
                                    continue
                                speciesMap = returnedVal[1]
                                product = returnedVal[0]

                        logging.info("REACTION : ")
                        if modifierType in list(reactionDict.keys()):
                            modifierType = reactionDict[modifierType]


                        elif modifierType !="":
                            logging.info("Modifier type not translatable")
                            f = open(expStatementFile, 'a')
                            # f.write("RELATIONSHIP: " + str(relationship) + "|" + evidence + "\n")
                            reactionEntry = str(belStatementNumber) + "\t" + str(
                                relationship) + "\t" +"REACTION ADDED WITHOUT MOIDIFER\t"+ reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace( "\n", " ").replace("\t", " ") + "\n"
                            f.write(reactionEntry)
                            f.close()
                            modifierType=""
                            modifier=""

                        if relationship in list(reactionDict.keys()):
                            relationship = reactionDict[relationship]
                            reactionIdentifier += 1
                            reactionEntry = "r" + str(
                                reactionIdentifier) + "\t" + relationship + "\t" + reactant + "\t" + modifier + "\t" + product + "\t" + modifierType + "\t" +finalreference + "\t" + species +  "\t"+disease +"\t" + locationAnnotation+ "\t" +";".join(otherAnnotation) + "\n"
                            logging.info(reactionEntry)
                            reactionFile = open("outputs/reactions.txt", 'a')
                            reactionFile.write(reactionEntry)
                            reactionFile.close()
                        else:
                            logging.info("Relationship type not translatable")
                            alreadyIn = [line.strip() for line in open("outputs/additional/toMapReactions.txt", "r")]
                            if relationship not in alreadyIn:
                                f = open("outputs/additional/toMapReactions.txt", "a")
                                f.write(relationship + "\n")
                                f.close()
                            f = open(expStatementFile, 'a')
                            reactionEntry = str(belStatementNumber) + "\t" + str(relationship) +"\t"+ "NODE is SNP or undetified\t" + reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace("\n"," ").replace("\t"," ") + "\n"
                            f.write(reactionEntry)
                            f.close()
                            continue


            logging.info("#########################################################################################################\n")
            logging.info("Total BEL Statements: "+str(belStatementNumber))
            logging.info("\nTotal statements converted to reaction: "+str(reactionIdentifier))
        except Exception as ex:
            logging.info(" !!!EXCEPTION!!! : " + str(ex.args))
            f = open(expStatementFile, 'a')
            reactionEntry = str(belStatementNumber) + "\t" + str(relationship) + ""+"\t" + reactant + "\t " + product + "\t" + modifier + "\t" +finalreference + " | " + evidence.replace("\n"," ").replace("\t"," ") + "\n"
            f.write(reactionEntry)
            f.close()



# Convert file
# /home/alex/PhD/Bel2CellD/full_abstract3.xbel
#convertXBEL('/home/alex/PhD/Bel2CellD/small_corpus.xbel')
logfile= "logs/"+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")+"_log.txt"
# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG, format='%(message)s', datefmt='%m-%d %H:%M', filename=logfile,filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


xBEL2CellD().convertXBEL('/home/alex/PhD/APP/Aetionomy_AD.xbel')
logging.info("Done")