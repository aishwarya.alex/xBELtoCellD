TERM	TYPE	MERGE
Neurons	CELL	
Endosome	COMPARTMENT	
Endosomes		Endosome
Astrocytes	CELL	
Endoplasmic Reticulum	COMPARTMENT	
Muscles	CELL	
Microglia	CELL	
Mitochondria	COMPARTMENT	
SH-SY5Y	CELL	
293	CELL	
Synapse	COMPARTMENT	
Synapses		Synapse
Centrosome	COMPARTMENT	
App transgenic	CELL	
Tissues		
Beta cell	CELL	
primary cortical neuron	CELL	
Endothelium	TISSUE	
INS-1 cells	CELL	
PC-12	CELL	
Cell Membrane	COMPARTMENT	
Endothelial Cells	CELL	
Cytosol	COMPARTMENT	
Cell Line		
Extracellular Matrix	COMPARTMENT	
Lymphocytes	CELL	
primary neuron	CELL	
Cytoplasm	COMPARTMENT	
Myocytes, Smooth Muscle	CELL	
N2a695 cell	CELL	
Neuroblastoma cell	CELL	
Leukocytes	CELL	
Prostate	TISSUE	
Neural Stem Cells	CELL	
Blood Platelets	CELL	
Cell Surface Extensions	COMPARTMENT	
Fibroblasts	CELL	
Intracellular Space	COMPARTMENT	
CHOAPPsw	CELL	
Cell Nucleus	COMPARTMENT	
Erythrocytes	CELL	
Human Umbilical Vein Endothelial Cells	CELL	
Pyramidal Cells	CELL	
Muscle, Skeletal	TISSUE	
Astrocyte		Astrocytes
Brain	TISSUE	
Endosome	COMPARTMENT	
trans-Golgi Network	COMPARTMENT	
Extracellular Space	COMPARTMENT	
Neurite	COMPARTMENT	
Neurites		Neurite
Blood		Blood Cells
Blood Cells	CELL	
Bone Marrow Cells	CELL	
Cell Membrane Structures		Cell Membrane
Cerebrospinal Fluid	TISSUE	
Lysosome	COMPARTMENT	
Lysosomes		Lysosome
Blood-Brain Barrier	TISSUE	
NERVOUSSYSTEM:Blood-Brain Barrier		Blood-Brain Barrier
Cholinergic Neurons	CELL	
NERVOUSSYSTEM:Cholinergic Neurons		Cholinergic Neurons
Dopaminergic Neurons	CELL	
NERVOUSSYSTEM:Dopaminergic Neurons		Dopaminergic Neurons
NERVOUSSYSTEM:Nervous System	TISSUE	Nervous System
NERVOUSSYSTEM:Neurofibrillary Tangles	COMPARTMENT	Neurofibrillary Tangles
NERVOUSSYSTEM:Prefrontal Cortex	TISSUE	Prefrontal Cortex
NERVOUSSYSTEM:Prosencephalon	TISSUE	Prosencephalon
PATIENT:AD T2DM -ve	ORGANISM	PATIENT:AD T2DM -ve
USERDEFINEDCELLLINE:NT2N cells	CELL	NT2N cells
FDASTATUS:Phase 3		
NERVOUSSYSTEM:CA1 Region, Hippocampal	TISSUE	CA1 Region, Hippocampal
NERVOUSSYSTEM:Cerebellum	TISSUE	Cerebellum
NERVOUSSYSTEM:CA2 Region, Hippocampal	TISSUE	CA2 Region, Hippocampal
NERVOUSSYSTEM:Neurites		Neurite
NERVOUSSYSTEM:Spinal Cord	TISSUE	Spinal Cord
USERDEFINEDCELL:Astrocyte		Astrocytes
USERDEFINEDCELLLINE:CHOAPPsw		CHOAPPsw
NERVOUSSYSTEM:Interneurons	CELL	Interneurons
FLUIDANDSECRETION:Bodily Secretions	TISSUE	Bodily Secretions
FLUIDANDSECRETION:Serum	TISSUE	Serum
NERVOUSSYSTEM:Astrocytes		Astrocytes
NERVOUSSYSTEM:Central Nervous System	TISSUE	Central Nervous System
USERDEFINEDCELLLINE:Neuroblastoma cell		Neuroblastoma cell
USERDEFINEDCELLLINE:primary neuron		primary neuron
NERVOUSSYSTEM:Autonomic Fibers, Postganglionic	TISSUE	Autonomic Fibers, Postganglionic
NERVOUSSYSTEM:Hypothalamus	TISSUE	Hypothalamus
CARDIOVASCULARSYSTEM:Blood Vessels	TISSUE	Blood vessels
CARDIOVASCULARSYSTEM:Microvessels	TISSUE	Microvessels
FDASTATUS:Phase 2		
USERDEFINEDCELLLINE:N2a695 cell	CELL	N2a695 cell
NERVOUSSYSTEM:Microglia	CELL	Microglia
DISEASESTATE:Mild AD		
PATIENT:AD T2DM +ve	ORGANISM	PATIENT:AD T2DM +ve
PATIENT:APOE e4 +ve	ORGANISM	PATIENT:APOE e4 +ve
NERVOUSSYSTEM:Cerebral Cortex	TISSUE	Cerebral Cortex
NERVOUSSYSTEM:Motor Neurons	CELL	Motor Neurons
USERDEFINEDCELLLINE:App transgenic		App transgenic
DIGESTIVESYSTEM:Islets of Langerhans	TISSUE	Islets of Langerhans
USERDEFINEDCELLLINE:primary cortical neuron		primary cortical neuron
USERDEFINEDSPECIES:C elegans	ORGANISM	C elegans
NERVOUSSYSTEM:Cerebrum	TISSUE	Cerebrum
USERDEFINEDCELLLINE:INS-1 cells		INS-1 cells
FLUIDANDSECRETION:Cerebrospinal Fluid		Cerebrospinal Fluid
NERVOUSSYSTEM:Hippocampus	TISSUE	Hippocampus
FLUIDANDSECRETION:Plasma	TISSUE	Plasma
USERDEFINEDCELLLINE:IDE APP transgenic	CELL	IDE APP transgenic
KNOCKOUTMICE:IDE KO mice	ORGANISM	IDE KO mice
USERDEFINEDSPECIES:drosophila	ORGANISM	drosophila
USERDEFINEDGENDER:Female		
FLUIDANDSECRETION:Blood		Blood Cells
DIGESTIVESYSTEM:Liver	TISSUE	Liver
USERDEFINEDCELL:Beta cell		Beta cell
NERVOUSSYSTEM:Neurons		Neurons
PATIENT:APOE e4 -ve	ORGANISM	PATIENT:APOE e4 -ve
NERVOUSSYSTEM:Brain		Brain
