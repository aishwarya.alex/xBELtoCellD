# Convertor from xBEL to CellDesigner.
## Input: 
1. .XBEL file *(to be provided)*
Several mapping files already provided, can be updated to include any user defined entity mapping
2. abundanceMapping.txt  : Mapping for abundance entities with namespaces
3. belSpeciesNestedAnnotationsMapping.txt : Mapping for nested elements, added as additional GO:annotation for elements eg: peptidaseActivity
4. desc2012.xml : MeSH xml file to create db, will be downloaded if doesn't exist
5. mesh_desc2012.txt : Will be created if doesn't exist, file to create sqlitedb
5. entityMapping.txt : Mapping for entity names from BEL to SBML
6. locationMapping.txt : Map for merging and mapping to location type: CELL, TISSUE or COMPARTMENT
7. namespaceMapping.txt : BEL namespaces to miriam urn mapping
8. reactionMapping.txt : BEL predicates to SBML reaction types

## Output:
nodes.txt (nodes with anotation and location information)

reactions.txt (reactions, with nodes referring to nodes.txt and annotation)


## Usage:
xBCD=xBEL2CellD()

xBCD.convertXBEL('/home/alex/PhD/APP/Aetionomy_AD.xbel')